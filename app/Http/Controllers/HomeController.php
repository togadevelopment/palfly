<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $requestContent = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ]
        ];
        $url = 'http://192.168.1.167/api/WFchxBSkNMKEWNWTap9H-SHAoEC41y0b6uWHQzyH/lights/1';
        $connection = new Client();
        $call =  $connection->get($url,$requestContent);
        $response =  json_decode($call->getBody(),true);
        $data['response'] = $response;

        return view('home',$data);

    }
}
