<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\User;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

//    public function login()
//    {
//        $credentials = request()->validate([
//            'email' => 'required|email',
//            'password' => 'required',
//        ]);
//
//        $user = User::where('email', $credentials['email'])->first();
//
//        if (! $user || ! Hash::check($credentials['password'], $user->password)) {
//            return response()->json(['message' => 'Unauthorized'], 401);
//        }
//
//        return $this->respondWithToken($user->createToken('token-name'), ["user" => $user]);
//    }



    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user = User::where('email', $credentials['email'])->first();
            $data['token'] =$user->createToken('token-name')->plainTextToken;
            // Authentication passed...
            return redirect()->intended('home')->with('data',$data);
        }
    }




}
