import axios from 'axios'

export default {

    namespaced:true,

    state : {
        authenticated: false,
        user: {name:null},
        token:null
    },

 getters:  {
    authenticated: state => {
        return state.authenticated
    },
    user : state => {
        return state.user
    },
     username : state => {
        if(state.user)
         return state.user.name
     },
},

 mutations : {
    SET_AUTHENTICATED (state, value) {
        state.authenticated = value
    },

    SET_USER (state, value) {
        state.user = value
    },
     SET_TOKEN(state,value){
        state.token =value
     }
},

actions :{
    async signIn (ctx, credentials) {
        await axios.get('/sanctum/csrf-cookie')
        await axios.post('/login', credentials)

        //return dispatch('me')
    },


    async signOut ({ dispatch }) {
        await axios.post('/logout')

        return dispatch('me')
    },

    me ({ commit },payload) {
        console.log(payload[0].token)
            commit('SET_AUTHENTICATED', true)
            commit('SET_USER', payload[0].user)
            commit('SET_TOKEN', payload[0].token)
    }
},


}
