import axios from 'axios'


export default {

    namespaced:true,
    state : {
        returnTips: [{}],
        oneWayTrips: [{}],
        destination:'',
        leave_date:'',
        return_date:'',
        ReturnFormValided:false,
        OneWayFormValided:false
    },
 getters:  {
     returnTips: state => {
        return state.returnTips
    },
     oneWayTrips : state => {
        return state.oneWayTrips
    },
     destination : state => {
        if(state.destination)
         return state.destination
     },
     leaveDate : state => {
         if(state.leave_date)
             return state.leave_date
     },
     returnDate : state => {
         if(state.return_date)
             return state.return_date
     },
     validedForm :state=>{
             return state.ReturnFormValided
     },
     validedOneWayForm :state=>{
         return state.OneWayFormValided
     }

},

 mutations : {
     SET_RETURN (state, value) {
         return  state.returnTips.push(value)
    },
     UPDATE_RETURN(state,value){
         let id =   state.returnTips.findIndex(x => x.key == value.key);
         return   state.returnTips[id] = value
     },
     UPDATE_ONEWAY(state,value){
         let id =   state.oneWayTrips.findIndex(x => x.key == value.key);
         return  state.returnTips[id] = value
     },
     SET_ONEWAY (state, value) {
         return  state.oneWayTrips.push(value)
    },
     SET_DEPARTDATE(state,value){
         return state.leave_date = value
     },
     SET_RETURNDATE(state,value){
         return state.return_date = value
     },
     SET_DESTINATION(state,value){
         return state.destination = value
     },
     REMOVE_R_GROUP(state,value){
         return state.returnTips.splice(value, 1)
     },
     REMOVE_OW_GROUP(state,value){
         return state.returnTips.splice(value, 1)
     },

     REHYDRATE_GROUP_SEARCH(state,value){

         let id =   state.returnTips.findIndex(x => x.key == value.key);
         console.log(value.key+'--INDEX IS--'+id)

         return state.returnTips.find(item => item.key == value.key);
     },

     CHECK_FORM_SEARCH_RETURN(state,value){
         let groups =  state.returnTips
         let cnt = 0;
         console.log(groups.length);
         if(groups.length > 1  && state.leave_date && state.return_date){
             console.log(false)
             state.ReturnFormValided = true
         }
         else {
             console.log(true)
             state.ReturnFormValided = false
         }
     },



     CHECK_FORM_SEARCH_ONEWAY(state,value){
         let groups =  state.oneWayTrips
         let cnt = 0;


         console.log(groups.length);

         if(groups.length >= 1  && state.leave_date){
             console.log(false)
             state.OneWayFormValided = true
         }
         else {
             console.log(true)
             state.OneWayFormValided = false

         }
     },

     CKECK_GROUPS(state,value){
        if(state.returnTips.indexOf(value.key) > -1  || state.oneWayTrips.indexOf(value.key) > -1){
            console.log('found')
        }
     },

},

actions :{
     addGroupToReturn ({commit}, payload) {
         commit('SET_RETURN', payload)
    },
    addGroupToOneWay ({commit}, payload) {
        commit('SET_ONEWAY', payload)
    },
    AmendGroupToReturn ({commit}, payload) {
        commit('UPDATE_RETURN', payload)
    },
    AmendGroupToOneWay ({commit}, payload) {
        commit('UPDATE_ONEWAY', payload)
    },
    DepartureDate({commit},payload){
         commit('SET_DEPARTDATE',payload)
    },
    ReturnDate({commit},payload){
        commit('SET_RETURNDATE',payload)
    },
    Destination({commit},payload){
        commit('SET_DESTINATION',payload)
    },
    RemoveReturningGroup({commit},payload){
        commit('REMOVE_R_GROUP',payload)
    },
    RemoveOneWayGroup({commit},payload){
        commit('REMOVE_OW_GROUP',payload)
    },
   async RehydrateGroupSearch({commit},payload){
       commit('REHYDRATE_GROUP_SEARCH',payload)
    },
    CheckFormReturn({commit},payload){
        commit('CHECK_FORM_SEARCH_RETURN',payload)
    },
    CheckFormOneway({commit},payload){
        commit('CHECK_FORM_SEARCH_ONEWAY',payload)
    }

},


}
