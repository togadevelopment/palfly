import Vue from 'vue';
import Vuex from 'vuex';



Vue.use(Vuex);

import auth from "./modules/auth";
import modules from "./modules";


export default  new Vuex.Store({
strict:true,
   modules
    });
