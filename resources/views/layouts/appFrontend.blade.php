@php
    $config = [
        'appName' => config('app.name'),
        'locale' => $locale = app()->getLocale(),
        'locales' => config('app.locales'),
        'auth'=>Auth::User()
    ];
@endphp
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
	
	



	
</head>
<body>
@guest
    <div id="app">
    <header id="header">
        <div class="container main-menu">
            <div class="row align-items-center justify-content-between d-flex">
                <div id="logo"> <a href="{{ url('/') }}"><img src="{{asset('/img')}}/logo.png" alt="" title=""></a> </div>
                <nav id="nav-menu-container">
                    <ul class="nav-menu">
                        <li class="nav-item ">
                            <router-link to="/" class="nav-link"> Home</router-link>
                        </li>
						<li><a href="//www.instagram.com/Palfly_travel/"><img src="img/instagram-icon.png"></a>
						<li><a href="//www.facebook.com/Palfly_travel"><img src="img/facebook-icon.png"></a></li>
						<li><a data-toggle="modal" data-target="#exampleModal">Newsletter</a></li>
							
					
                        @guest
						
						<!--
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
						-->
                            @if (Route::has('register'))
						<!--
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
						-->
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </nav>
                <!-- #nav-menu-container -->
            </div>
        </div>
    </header>

@endguest


@yield('content')

</div>
	
	
	<!-- MODAL -->
	
	
	
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Newsletter Signup</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
	<!-- Begin Mailchimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
	/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="https://palfly.us19.list-manage.com/subscribe/post?u=ff618631a75f2d0c916f2dcfe&amp;id=a8dbc5804c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<h2>Sign-up for ideas, offers &amp; travel news :)</h2>
<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
<div class="mc-field-group">
	<label for="mce-FNAME">Name  <span class="asterisk">*</span>
</label>
	<input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
</div>
<div class="mc-field-group">
	<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
</label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_ff618631a75f2d0c916f2dcfe_a8dbc5804c" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[0]='EMAIL';ftypes[0]='email';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
		
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
	
	
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" ]></script>
{{-- Global configuration object --}}
<script>
    window.config = @json($config);
    // Select the node that will be observed for mutations

</script>
<!-- Custom scripts for all pages-->
<script src="{{ asset('/') }}js/jquery-ui.js"></script>
<script src="{{ asset('/') }}js/easing.min.js"></script>
<script src="{{ asset('/') }}js/hoverIntent.js"></script>
<script src="{{ asset('/') }}js/superfish.min.js"></script>
<script src="{{ asset('/') }}js/jquery.ajaxchimp.min.js"></script>
<script src="{{ asset('/') }}js/jquery.magnific-popup.min.js"></script>
<script src="{{ asset('/') }}js/jquery.nice-select.min.js"></script>
<script src="{{ asset('/') }}js/owl.carousel.min.js"></script>
<script src="{{ asset('/') }}js/main.js"></script>


	
	<script>
function showMailingPopUp() {
require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"http://www.palfly.com/","uuid":"ff618631a75f2d0c916f2dcfe","lid":"a8dbc5804c"}) })
document.cookie = "MCPopupClosed=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
};
document.getElementById("open-popup").onclick = function() {showMailingPopUp()};
</script>
</body>
</html>
