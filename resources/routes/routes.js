// // import Home from './components/Home.vue';
// import Example from '../js/components/ExampleComponent.vue';
// import Register from "../js/components/Auth/Register";
// import Login from "../js/components/Auth/Login";

import Profile from "../js/components/admin/pages/Profile";
import MainPage from "../js/pages/MainPage";
import ResultsPage from "../js/pages/ResultsPage";
export const routes = [
    // // { path: '/app', component: Example, name: 'Example' },
    // {path:'/app/example', component: Example, name: 'Example' },
    // {path:'/app/register',name:'register',component:Register},
    // {path:'/app/login',name:'login',component:Login},
    {
        path:'',
        name:'welcome',
        component: MainPage,
        props: true,

    },
    {
        path:'/',
        name:'welcome',
        component: MainPage,
        props: true,

    },
    {
        path:'/results',
        name:'results',
        component: ResultsPage,
        props: true,

    },

    {
        path:'home/:all',
        name:'home',
        props: true,

    },
    {
        path:'/home/profile',
        name:'profile',
        component:Profile,
        props: true,

    },


];
